# Golang profile helper

Use signal to helper with golang pprof. 

## Usage

Import this package, and you will be ready:

```go
import _ "gitee.com/jiaoyuedave/go-sigprof/pprof"
```

Once import, program will be waiting for SIGUSR1 for cpu profile and SIGUSR2 for memprofile.

Start cpu profile:

```
kill -SIGUSR1 <pid>
```

Send this signal again to stop cpu profile.

Write memory profile:

```
kill -SIGUSER2 <pid>
```

Profile will write to `cpu` and `mem` directory of the excutable path by default, you can change it by setting `PPROF_PATH` environment.

To show profile:

```
go tool pprof <profile>
```

see: https://github.com/google/pprof/blob/main/doc/README.md.

