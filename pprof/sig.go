package pprof

import (
	"fmt"
	"os"
	"os/signal"
	"path"
	"runtime/pprof"
	"syscall"
	"time"
)

var pprofPath = os.Getenv("PPROF_PATH")

func cpuProfile(start bool) {
	if start {
		filename := fmt.Sprintf("cpuprofile-%d-%s.prof", os.Getpid(), time.Now().Format("060102150405"))
		f, err := createFile(pprofPath, "cpu", filename)
		if err != nil {
			fmt.Println(err)
			return
		}
		if err := pprof.StartCPUProfile(f); err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println("start cpu profile")
	} else {
		pprof.StopCPUProfile()
		fmt.Println("stop cpu profile")
	}
}

func memProfile() {
	filename := fmt.Sprintf("memprofile-%d-%s.mprof", os.Getpid(), time.Now().Format("060102150405"))
	f, err := createFile(pprofPath, "mem", filename)
	if err != nil {
		fmt.Println(err)
		return
	}
	if err := pprof.WriteHeapProfile(f); err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("write mem profile")
	f.Close()
}

// createFile creates a file and makes sure the directory exists
func createFile(elem ...string) (*os.File, error) {
	dir := path.Join(elem[:len(elem)-1]...)
	if err := os.MkdirAll(dir, 0755); err != nil {
		return nil, err
	}
	return os.Create(path.Join(elem...))
}

func init() {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGUSR1, syscall.SIGUSR2)
	go func() {
		var startCpuProfile bool
		for s := range sig {
			// handle signal
			switch s {
			case syscall.SIGUSR1:
				startCpuProfile = !startCpuProfile
				cpuProfile(startCpuProfile)
			case syscall.SIGUSR2:
				memProfile()
			default:
				fmt.Printf("unexpected signal: %v\n", s)
			}
		}
	}()
}
